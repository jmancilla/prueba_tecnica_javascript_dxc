const express = require("express");
const bodyParser = require('body-parser');
const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

let response = {
    data: {
        suma: Number,
        resta: Number,
        multiplicacion: Number,
        division: Number
    },
    errors: []
};

let validate;
let arreglo;

app.route('/test')
    .post(function(req, res) {
        if (Array.isArray(req.body.data)) {

            let arreglo = req.body.data;

            for (let i = 0; i < arreglo.length; i++) {
                if (Number(arreglo[i])) {
                    validate = false;
                } else {
                    validate = true;
                    break;
                };
            }

            if (validate === false) {
                response = {
                    data: {
                        suma: arreglo.reduce(function(a, b) { return a + b; }),
                        resta: arreglo.reduce(function(a, b) { return a - b; }),
                        multiplicacion: arreglo.reduce(function(a, b) { return a * b; }),
                        division: arreglo.reduce(function(a, b) { return a / b; })
                    },
                    errors: []
                }
            } else {
                response = {
                    data: "",
                    errors: [422, 'El arreglo recibido no es valido']
                }
            }

        } else {
            response = {
                data: req.body,
                errors: [422, 'No es un Array, se espera un arreglo data']
            }
        }
        res.send(response);
    })


app.use(function(req, res, next) {
    response = {
        data: "",
        errors: ['Ups!, Algo ha Fallado']
    };
    res.status(500).send(response);
});


app.listen(3000, () => {
    console.log("El servidor está inicializado en el puerto 3000");
});