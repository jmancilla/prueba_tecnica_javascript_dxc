# prueba_tecnica_javascript_dxc

Se realizo la app segun prueba de NodeJS con la libreria de Express,Body-parser y TypeScript.

Dicha API recibe un archivo tipo JSON con un arreglo que se llame 'data'.

1- Se verifica la URL que sea test en caso contrario emite error de URL invalida

2- Se verifica si el archivo recibido es un array, en caso contrario retorna error 422

3- De ser un array, se valida que el mismo solo contenga numeros, en caso contrario retorna error 422

4- De cumplirse los anteriores, retorna el response con la suma, resta, mutiplicacion y division de todo el arreglo

5- Las operaciones se realizaron con la funcion reduce de js

Las pruebas unitarias se realizaron con Postman emitiendo peticiones tipo POST al http://localhost:3000/test

